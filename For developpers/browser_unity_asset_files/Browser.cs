//add this at begining  

using System.IO;
using Newtonsoft.Json ;

///////////////////
///////////////////
// add this after 	public static string LocalUrlPrefix ......
///////////////////
///////////////////
	public Dictionary<string,List<KeyValuePair<string,string>>>   localStorage ;
	string domain;
	public bool hasDoneStorage;

///////////////////
///////////////////
// add this at the begining of the Awake function 
///////////////////
///////////////////
	if( GameObject.Find("settings"))
			localStorage = GameObject.Find("settings").GetComponent<settings>().sp.browser.localStorage;
		_url= PlayerPrefs.GetString("defaultWebsite");



///////////////////
///////////////////
// add this in updateCursor function after :  var cursorType = BrowserNative.zfb_getMouseCursor(browserId, out w, out h);
///////////////////
///////////////////

		((GUIBrowserUI)UIHandler).cursor=cursorType;


///////////////////
///////////////////
// add this at the begining of the OnItemChange function  
///////////////////
///////////////////
		modifyLinksInPage();

///////////////////
///////////////////
// add this at the end of the file (before the end of the class and the namespace )
///////////////////
///////////////////

		  public void modifyLinksInPage()
    {
    	//  make that links does not open in a new tab 
       	EvalJS("  var links = document.getElementsByTagName('a'); for(var i=0; i<links.length; i++) {  links[i].target='_self';}");

    	EvalJS(" document.storage0= '' ; for(var i=0;i< localStorage.length ;i++) { document.storage0= document.storage0 +';'+  localStorage.key(i)+ '*' +localStorage.getItem( localStorage.key( i ) ) ;  }");

       	EvalJS("document.storage0").Then(result => interpretStorage(result));
       	
       	// save the local starage and put it again 
 		 
    }
    public void interpretStorage(string result)
    {
    	

    	if(hasDoneStorage)
    	{


    	List<KeyValuePair<string,string>> l= new List<KeyValuePair<string,string>>();
    	string[] items = 	result.Split(';');


    	// if we already have a storage for this website
		if (localStorage.ContainsKey(domain)) {

			for(int i=0; i< items.Length;i++)
       		{
				string[] i1 = items[i].Split('*');
				if(i1.Length>1)
				{

					KeyValuePair<string,string> k = new KeyValuePair<string,string>(i1[0],i1[1]);

					int index =localStorage[domain].FindIndex(a => a.Key == k.Key);
				

					if(index==-1) // if the key does not exist, create it 
					{
						localStorage[domain].Add(k);
					}
					else{ // otherwize, modify it
						l=localStorage[domain];
						l[index] = k;
					}
				}
       		}       

		 	//localStorage[_url].AddRange(l);
		}
		else { // if this is a new website create a new entry

       	for(int i=0; i< items.Length;i++)
       	{
			string[] i1 = items[i].Split('*');
			if(i1.Length>1)
			{
				//KeyValuePair<string,string> k = new KeyValuePair<string,string>("a","b");
				l.Add(new KeyValuePair<string,string>(i1[0],i1[1]));
			}
       	}       

			localStorage.Add(domain,l);
		}
	}

    }
        public void setStorage()
    {
    	 	EvalJS("document.domain").Then(result =>  setStorage2(result) );

    }
    public void setStorage2(string result)
    {
    		domain = result;
    	// if we already have a storage for this website
		if (localStorage.ContainsKey(domain) ) {
			foreach( var item in localStorage[domain])
			{
				EvalJS("localStorage.setItem("+item.Key+","+item.Value+");");
			}
		}
		hasDoneStorage= true;
		LoadURL(_url, false) ;
    }
    public string displayLocalStorage()
    {
    		 return JsonConvert.SerializeObject(localStorage);
    }