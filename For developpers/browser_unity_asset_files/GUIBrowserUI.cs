using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Tobii.Gaming;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;

using System.Runtime.InteropServices;

namespace ZenFulcrum.EmbeddedBrowser
{

    /** Attach this script to a GUI Image to use a browser on it. */
    [RequireComponent(typeof(RawImage))]
    [RequireComponent(typeof(Browser))]
    public class GUIBrowserUI :
        MonoBehaviour,
        IBrowserUI,
        ISelectHandler, IDeselectHandler,
        IPointerEnterHandler, IPointerExitHandler,
        IPointerDownHandler
    {
        protected RawImage myImage;
        protected Browser browser;
        private Vector2 positionMouse;
        public GameObject keyboard;
        public BrowserNative.CursorType cursor;
        public Vector3 saveScale, savePos;
        private int tmpVar = 0;
        private int aknowledged = 0;
        public bool isMini = false;
        public bool enableInput = true;

        bool setCookies;
        bool isNewURL;
        bool hasDoneStorage = false;
        //mouse events for windows 
        /*[DllImport("user32.dll")]
        public static extern int SetCursorPos(int x, int y);*/
        [DllImport("user32.dll")]
        static extern void mouse_event(int dwFlags, int dx, int dy,
                          int dwData, int dwExtraInfo);

        [DllImport("user32.dll")]
        public static extern int SetCursorPos(int x, int y);


        public bool isChecked = false;
        protected void Awake()
        {

            BrowserCursor = new BrowserCursor();
            InputSettings = new BrowserInputSettings();

            browser = GetComponent<Browser>();
            myImage = GetComponent<RawImage>();

            browser.afterResize += UpdateTexture;
            browser.UIHandler = this;
            BrowserCursor.cursorChange += () => {
                SetCursor(BrowserCursor);
            };

            rTransform = GetComponent<RectTransform>();
            //positionMouse= new Vector2(GameObject.Find("BrowserGUI").transform.position.x, GameObject.Find("BrowserGUI").transform.position.y);
            //	SetCursorPos((int)positionMouse.x,(int)positionMouse.y);	
            /*
                    if( GameObject.Find("settings"))
                    {
                        settings set = GameObject.Find("settings").GetComponent<settings>() ;
                        browser.cookies = set.browser.cook;
                    }*/
            setCookies = false;
            isNewURL = false;

            GameObject.Find("wholeWindow").transform.localScale = new Vector3(0.00085f * Screen.width, 0.002f * Screen.height, 1.0f);
            //	this.transform.localScale =   new Vector3(0.64f*scale.x , 0.64f*scale.y , 1.0f*scale.z);
            //	browser.resize(this.transform.width*0.64f*scale.x , this.transform.width*0.64f*scale.y);
            //   browser.Resize(1500, 500);


        }

        protected void OnEnable()
        {
            StartCoroutine(WatchResize());
        }

        /** Automatically resizes the browser to match the size of this object. */
        private IEnumerator WatchResize()
        {
            Rect currentSize = new Rect();


            while (enabled)
            {
                var rect = rTransform.rect;
                Vector2 size = new Vector2(rect.size.x * GameObject.Find("wholeWindow").transform.localScale.x, rect.size.y * GameObject.Find("wholeWindow").transform.localScale.y);
                //if (size.x <= 0 || size.y <= 0) rect.size = new Vector2(512, 512);
                if (rect != currentSize)
                {
                    browser.Resize((int)size.x, (int)size.y);
                    currentSize = rect;
                }
                //	}

                yield return null;
            }
        }

        protected void UpdateTexture(Texture2D texture)
        {
            myImage.texture = texture;
            myImage.uvRect = new Rect(0, 0, 1, 1);
        }

        protected List<Event> keyEvents = new List<Event>();
        protected List<Event> keyEventsLast = new List<Event>();
        protected BaseRaycaster raycaster;
        protected RectTransform rTransform;
        //	protected List<RaycastResult> raycastResults = new List<RaycastResult>();

        public virtual void InputUpdate()
        {


            if (!browser.IsLoaded)
            {
                isNewURL = true;
            }
            if (isNewURL && browser.IsLoaded)
            {
                //SetCursorPos(800,500);
                //Vector2 centerBrowser = new Vector2(transform.position.x , transform.position.y);
                GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().mouseGoToPosition(transform.position);




                GameObject.Find("BrowserGUI").GetComponent<Browser>().Zoom = 3;
                //GameObject.Find("BrowserGUI").GetComponent<GUIBrowserUI>().scroll(30);
                scroll(30);



                isNewURL = false;
                if (!hasDoneStorage)
                {
                    hasDoneStorage = true;
                    browser.setStorage();

                }
            }

            if (browser.IsReady && !setCookies)
            {

                //        browser.CookieManager.ClearAll();

                IPromise<List<Cookie>> promise = GameObject.Find("BrowserGUI").GetComponent<Browser>().CookieManager.GetCookies();
                //   tmp.Then(result =>dothis(result) );
                promise.Then(result => browser.CookieManager.setCookies(GameObject.Find("settings").GetComponent<settings>().sp.browser.cookies));
                setCookies = true;
            }

            //modifyLinksInPage();




            /*if(isMini)
                //transform.localScale = new Vector3(0.00045f * Screen.width , 0.0005f * Screen.height , 1.0f);
            //	transform.localScale = new Vector3(0.00055f * Screen.width , 0.00063f * Screen.height , 1.0f);
            else
            {
                //transform.localScale = new Vector3(0.0007f * Screen.width, 0.0014f * Screen.height , 1.0f);
                //transform.localScale = new Vector3(0.0005f * Screen.width, 0.0014f * Screen.height , 1.0f);
            //	transform.localScale = new Vector3(0.00071f * Screen.width , 0.0017f * Screen.height , 1.0f);

            }*/
            //transform.localPosition = new Vector3( Screen.width/6,-100f,0);
            if (tmpVar == 1)
            {
                miniaturiseBrowser(true);
            }
            if (tmpVar > 0)
            {
                tmpVar--;
            }

            var tmp = keyEvents;
            keyEvents = keyEventsLast;
            keyEventsLast = tmp;
            for (int i = 0; i < keyEvents.Count; i++)

                if (aknowledged == 0)
                {
                    keyEvents.Clear();

                }

            //	if (MouseHasFocus) {
            if (!raycaster) raycaster = GetComponentInParent<BaseRaycaster>();

            //			raycastResults.Clear();

            //			raycaster.Raycast(data, raycastResults);

            //			if (raycastResults.Count != 0) {
            //				Vector2 pos = raycastResults[0].stuff
            Vector2 pos;
            RectTransformUtility.ScreenPointToLocalPointInRectangle(
                (RectTransform)transform, Input.mousePosition, raycaster.eventCamera, out pos
            );
            pos.x = pos.x / rTransform.rect.width;
            pos.y = pos.y / rTransform.rect.height;
            MousePosition = pos;

            var buttons = (MouseButton)0;
            if (Input.GetMouseButton(0)) buttons |= MouseButton.Left;
            if (Input.GetMouseButton(1)) buttons |= MouseButton.Right;
            if (Input.GetMouseButton(2)) buttons |= MouseButton.Middle;
            MouseButtons = buttons;

            if (aknowledged == 0)
            {
                //MouseScroll = new Vector2(0,0);
                MouseScroll = Input.mouseScrollDelta;
                //MouseButtons = 0;
            }

            //} else {
            //}
            //Unity doesn't include events for some keys, so fake it.
            if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
            {
                //Note: doesn't matter if left or right shift, the browser can't tell.
                //(Prepend event. We don't know what happened first, but pressing shift usually precedes other key presses)
                keyEventsLast.Insert(0, new Event() { type = EventType.KeyDown, keyCode = KeyCode.LeftShift });
            }

            if (Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.RightShift))
            {
                //Note: doesn't matter if left or right shift, the browser can't tell.
                keyEventsLast.Add(new Event() { type = EventType.KeyUp, keyCode = KeyCode.LeftShift });
            }

            if (aknowledged > 0)
                aknowledged = aknowledged - 1;
        }

        public void OnGUI()
        {
            var ev = Event.current;
            if (ev.type != EventType.KeyDown && ev.type != EventType.KeyUp) return;

            keyEvents.Add(new Event(ev));
        }

        protected virtual void SetCursor(BrowserCursor newCursor)
        {
            if (!_mouseHasFocus && newCursor != null) return;

            if (newCursor == null)
            {
                Cursor.visible = true;
                Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
            }
            else
            {
                if (newCursor.Texture != null)
                {
                    Cursor.visible = true;
                    Cursor.SetCursor(newCursor.Texture, newCursor.Hotspot, CursorMode.Auto);
                }
                else
                {
                    Cursor.visible = false;
                    Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
                }
            }
        }

        protected bool _mouseHasFocus;
        public bool MouseHasFocus { get { return _mouseHasFocus && enableInput; } }
        public Vector2 MousePosition { get; private set; }
        public MouseButton MouseButtons { get; private set; }
        public Vector2 MouseScroll { get; private set; }
        protected bool _keyboardHasFocus;
        public bool KeyboardHasFocus { get { return _keyboardHasFocus && enableInput; } }
        public List<Event> KeyEvents { get { return keyEventsLast; } }
        public BrowserCursor BrowserCursor { get; private set; }
        public BrowserInputSettings InputSettings { get; private set; }

        public void OnSelect(BaseEventData eventData)
        {
            _keyboardHasFocus = true;
        }

        public void OnDeselect(BaseEventData eventData)
        {
            _keyboardHasFocus = false;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _mouseHasFocus = true;
            SetCursor(BrowserCursor);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _mouseHasFocus = false;
            SetCursor(null);
        }


        public void OnPointerDown(PointerEventData eventData)
        {
            EventSystem.current.SetSelectedGameObject(gameObject);
        }


        public void ExecuteKey(KeyCode key)
        {

            Event keypress = new Event();
            keypress.type = EventType.KeyDown;

            keypress.keyCode = key;
            keypress.character = '\0';


            Event keypress2 = new Event();
            keypress2.type = EventType.KeyUp;

            keypress2.keyCode = key;
            keypress2.character = '\0';

            KeyEvents.Add(keypress);
            KeyEvents.Add(keypress2);
            aknowledged = 1;
        }

        public void enterKey(int pressEnter)
        {

            // restore length browser
            miniaturiseBrowser(false);

            //dismiss keyboard
            if (GameObject.Find("keyboard"))
            {
                GameObject.Find("keyboard").SetActive(false);
            }
            if (pressEnter == 1)
            {
                // similate enter key
                ExecuteKey(KeyCode.Return);
                writeKey((char)13);


            }
            //	GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().readMode = false;
            GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().resetPointsBrowser();


        }
        public void click()
        {

            //down
            mouse_event((int)(0x00000002), 0, 0, 0, 0);
            //up
            mouse_event((int)(0x00000004), 0, 0, 0, 0);
            aknowledged = 3;
            // if we want to write, activate the keyboard and resize the browser , unless it is already there
            if (cursor == BrowserNative.CursorType.IBeam && !GameObject.Find("keyboard"))
            {
                tmpVar = 20;

            }
            GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().displayRadialPanel(false);

        }

        public void writeKey(char letter)
        {


            Event keypress2 = new Event();
            keypress2.type = EventType.KeyDown;

            keypress2.keyCode = KeyCode.None;
            keypress2.character = letter;

            KeyEvents.Add(keypress2);
            aknowledged = 1;
        }

        public void scroll(string key)
        {
            print("scroll1;");

            if (key == "0") // up
            {
                MouseScroll = new Vector2(0, 1f);
            }
            else if (key == "1") // right
            {
                MouseScroll = new Vector2(-1f, 0);
            }
            else if (key == "2") // down
            {
                MouseScroll = new Vector2(0, -1f);
            }
            else if (key == "3") // left
            {
                MouseScroll = new Vector2(1f, 0);
            }
            aknowledged = 1;
        }
        public void scroll(float valY)
        {
            print("scroll2;");

            /*
            if(valY<0)
                    MouseScroll=new Vector2(0,-1); 
            else
                    MouseScroll=new Vector2(0,1); 

            aknowledged= (int)Mathf.Abs(valY);	
            */
            MouseScroll = new Vector2(0, valY);
            aknowledged = 1;

        }

        public void miniaturiseBrowser(bool mini)
        {
            GameObject toscale = GameObject.Find("browser");

            if (mini)
            {

                browser.EvalJS("var elmnt = document.activeElement; elmnt.nodeName ").Then(result => goMini(result));

            }
            else
            {

                toscale.GetComponent<RectTransform>().anchorMin = new Vector2(0f, 0f);
                toscale.GetComponent<RectTransform>().anchorMax = new Vector2(0f, 0f);
                toscale.GetComponent<RectTransform>().pivot = new Vector2(0f, 0f);
                // GetComponent<BoxCollider>().center  = new Vector2(875f,300f);

                toscale.GetComponent<RectTransform>().localScale = saveScale;
                toscale.GetComponent<RectTransform>().localPosition = savePos;


                keyboard.SetActive(false);
                //GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().readMode = false ;
                isMini = false;
                GameObject.Find("BrowserGUI").GetComponent<Browser>().Zoom = 3;
                GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().mouseGoToPosition(transform.position);

                // 	transform.localPosition = new Vector3(transform.localPosition.x -12,transform.localPosition.y-12,transform.localPosition.z);
            }
        }

        public void goMini(string typeOfObject)
        {
            if (typeOfObject != "BODY") // if it is body it mean it was not a textfield or similar
            {

                GameObject toscale = GameObject.Find("browser");

                saveScale = toscale.GetComponent<RectTransform>().localScale;
                savePos = toscale.GetComponent<RectTransform>().localPosition;

                toscale.GetComponent<RectTransform>().anchorMin = new Vector2(1f, 1f);
                toscale.GetComponent<RectTransform>().anchorMax = new Vector2(1f, 1f);
                toscale.GetComponent<RectTransform>().pivot = new Vector2(1f, 1f);
                //GetComponent<BoxCollider>().center  = new Vector2(-875f,-300f);


                keyboard.SetActive(true);
                //GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().readMode = true ;
                toscale.GetComponent<RectTransform>().localPosition = new Vector3(250, 205, 0f);
                toscale.GetComponent<RectTransform>().localScale = new Vector3(0.61f, 0.4f, 1.0f);
                //titlesMenu.SetActive(false);
                //titilesScroll.SetActive(true);
                browser.Zoom = Mathf.Max(10, browser.Zoom);
                centerPage(typeOfObject);

                GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().mouseGoToPosition(new Vector2(transform.position.x + toscale.GetComponent<RectTransform>().localPosition.x, transform.position.y + toscale.GetComponent<RectTransform>().localPosition.y));

            }

        }
        public void centerPage(string typeOfObject)
        {
            // this work for most website, dont work for whatsapp web for some reason 
            if (typeOfObject == "INPUT" || typeOfObject == "TEXTAREA")
            {
                browser.EvalJS("var elmnt = document.activeElement;  var elementRect = elmnt.getBoundingClientRect(); var absoluteElementTop = elementRect.top + window.pageYOffset;var middleY = absoluteElementTop ;var absoluteElementLeft = elementRect.left + window.pageXOffset; var middleX = absoluteElementLeft; window.scrollTo(middleX, middleY);"); // put the field in view
            }
            else //if(typeOfObject!="BODY")
            {
                // this works for whasapp web ,  far from best programming, feel free to find a better solution ^^ 

                browser.EvalJS("var elmnt = document.activeElement;  elmnt.scrollIntoView();  ");

                for (int i = 0; i < 50; i++)
                {
                    writeKey('a');
                }
                for (int i = 0; i < 50; i++)
                {
                    ExecuteKey(KeyCode.Backspace);
                }
            }

        }

        public void zoomToPos()
        {
            print("zoom");
            Vector2 pos = new Vector2(GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().center.X, GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().center.Y);
            browser.Zoom = browser.Zoom + 1;

            float percent = ((pos.y - ((Screen.height - 117) / 2)) / (Screen.height - 117)) * 2;
            scroll(percent * 3);

        }
        public void updateZoom(int z)
        {
            browser.Zoom = browser.Zoom + z;

        }
    }
}