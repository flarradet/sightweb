using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class MyButton_autocompletion:MyButton
{
    keyboard_manager keyboard;

    public void Awake()
    {
        base.Awake();
        keyboard= GameObject.Find("keyboard").GetComponent<keyboard_manager>();
        onClick.AddListener(delegate() { writeWord(); });
    }
    public void writeWord()
    {

              keyboard.writeWord(GetComponentInChildren<Text>().text);
    }
}

