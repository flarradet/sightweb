using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UnityEngine.SceneManagement;
using System.Diagnostics;

public class keyboard_manager_speech : keyboard_manager
{

	private string gender, emotion;
	public Text voiceBtn;
	String[] emotions = new String[] { "happy", "sad", "angrySad", "neutral" };
	public Sprite[] emotionsImagesSelected;
	public Sprite[] emotionsImagesUnselected;
	public Button[] emotionsButtons;

	String lastSentence="";
    public InputField focus;
    public GameObject menu; 
        public GameObject keyboard; 

    void Start()
    {
    	Time.timeScale = 1; 

    	gender = "Feminile";
    	emotion = "happy";
    	isActivated = true;

    	updateAC();

    /*	NEED TO DISABLE REPEAT 


    for(int i=3; i<6;i++)
    	{
    		specialKeys[i].GetComponent<Button>().interactable= false;
    	}*/

    	position=0;

    }

    public override void setup(){
        focus.Select();
        focus.caretPosition=position ;
    }

    public void changeEmotion(int emotionNb)
    {
    	emotion = emotions[emotionNb];
    	for (int i = 0; i < 4; i++)
    	{
    		if (i != emotionNb)
    		emotionsButtons[i].image.overrideSprite = emotionsImagesUnselected[i];
    		else
    		emotionsButtons[i].image.overrideSprite = emotionsImagesSelected[i];

    	}
    }
    
        public void SetFocus(InputField i)
        {
        	focus = i;
        }
/*
        public void SetActiveFocus(InputField i)
        {
        	focus = i;
        	SetActive(true);
        	focus.MoveTextEnd(true);
        }

            public void SetActive(bool b)
    {
        if (b)
        {
            if (!isActive)
            {
                gameObject.GetComponent<Animator>().Rebind();
                gameObject.GetComponent<Animator>().enabled = true;
            }
        }
        else
        {
            if (isActive)
            {
                gameObject.GetComponent<Animator>().SetBool("Hide", true);
            }
        }

        isActive = b;
    }
*/

        public override void WriteKey(Text t)
        {
        	//if (isActivated)
        //	{

        		if(position == focus.text.Length)
        		{
        			focus.text +=    t.text ;
        		}
        		else{
        			focus.text =   focus.text.Substring(0, position) + t.text + focus.text.Substring( position,focus.text.Length-position);
        		}

        	
        	   position++;
        	   focus.caretPosition=position ;

        	   updateAC();
          //  }

        }
/*
        public void WriteSpecialKey(int n)
        {

        	if (n == 14 || isActivated)
        	{
        		float threshold;
        		switch (n)
        		{
                case 0: // delete letter 
                	deleteLetter();
                	break;
                case 1:  // play 
 	                say(focus.text);
                	break;
                case 2:
                	SwitchCaps();
                	break;
                case 3:
                	SetActive(false);
                	break;
                case 4:
                	SetKeyboardType(1);
                	break;
                case 5:
                	SetKeyboardType(2);
                	break;
                case 6: // previous letter 
                    if(position>0)
                    {
                        position --;
                        focus.caretPosition=position ;
                        updateAC();
                    }
                	break;
                case 7: //next position
                    if(position!= focus.text.Length)
                    {
                        position ++;
                        focus.caretPosition=position ;                        
                        updateAC();
                    }
                	break;
                case 8:
                	SetKeyboardType(0);
                	break;
                case 9: //threshold
                	threshold = GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait_main + 0.25f;
                	GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait_main = threshold;
                	GameObject.Find("thresholdText").transform.GetComponent<Text>().text = "Threshold : " + threshold;
                	break;
                case 10: // threshold
                	threshold = GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait_main - 0.25f;
                	threshold = Math.Max(0.25f, threshold);
                	GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait_main = threshold;
                	GameObject.Find("thresholdText").transform.GetComponent<Text>().text = "Threshold : " + threshold;
                	break;
                case 11: // change gender
	                if (gender == "Male")
	                {
	                	gender = "Female";
	                	voiceBtn.text = "Femminile";
	                }
	                else
	                {
	                	gender = "Male";
	                	voiceBtn.text = "Maschio";
	                }
	                break;
                case 12: // display/hide ball

                    GameObject.Find("eyeTracker").GetComponent<eye_tracker>().displayBall= !GameObject.Find("eyeTracker").GetComponent<eye_tracker>().displayBall;
                    if(GameObject.Find("eyeTracker").GetComponent<eye_tracker>().displayBall)
                        GameObject.Find("ballBtn").GetComponent<Button>().GetComponent<Image>().color = Color.red;
                    else
                        GameObject.Find("ballBtn").GetComponent<Button>().GetComponent<Image>().color = Color.white;
                     GameObject.Find("eyeTracker").GetComponent<eye_tracker>().gazePlot.SetActive(GameObject.Find("eyeTracker").GetComponent<eye_tracker>().displayBall);

	                break;
                case 13: // clear 
	                clear();
	                break;
                case 14: // sleep mode 
	                //isActivated = !isActivated;
	                for (int i=0;i< 3; i++)
	                {
	                	specialKeys[i].GetComponent<Button>().GetComponent<Image>().color = buttonColors[Convert.ToInt32(!isActivated)];
	                }

	                break;
                case 15: // delete word
	                deleteWord();
	                break;
                case 16: //repeat
	                say(lastSentence);
	                break;

            }
        }
      //  remplaceText();
    }
*/

    public void say(string text)
    {
            // NEED  ENABLE REPEAT 

        GameObject.Find("Key 34").GetComponent<Button>().interactable= true;


    	lastSentence= text ;

        string[] lines = System.IO.File.ReadAllLines(@"preferences.txt");
       string type = lines[3].Split(new string[] { "=" }, StringSplitOptions.None)[1];
       string language = lines[4].Split(new string[] { "=" }, StringSplitOptions.None)[1];
    


        print( "Name="+language);

        if(type=="Windows voice")
        {
            
            GameObject.Find("speech").GetComponent<WindowsVoice>().speak(text, "Name="+language);
 
        }
        else
        {

            Process proc1 = new Process();
            proc1.StartInfo.FileName = @"C:\Program Files (x86)\eSpeak\command_line\espeak.exe";
            proc1.StartInfo.UseShellExecute = false;

            proc1.StartInfo.CreateNoWindow= true;
            proc1.StartInfo.Arguments = "-v "+language+" \""+text+"\"";
            proc1.Start();
      
        }
    }
      

    public override void writeWord(string text)
    {

    //	if (isActivated)
    //	{

    		if(focus.text=="")
    		{
    			focus.text= text;
    			position = text.Length+1;

    			focus.text = focus.text+" ";
    			focus.caretPosition=position ;


        	}
            else{
            	String focusedWord =	ac.getFocusedWord(focus.text,position);

            	focus.text= focus.text.Substring(0, position - focusedWord.Length )+ text + focus.text.Substring(position,focus.text.Length - position);
            	position= position + text.Length - focusedWord.Length;
            	if(focus.text.Length==position ||  focus.text[position]!=' ')
            	{    focus.text = focus.text+" ";
            	position++;
                }
     //   }

    	focus.caretPosition=position ;

        updateAC();
        
    }
      //  remplaceText();
}
    /*
    public void remplaceText(){

		if(focus.text!="")
		{
			focus.text= focus.text;
		
			if(focus.text[focus.text.Length -1]== ' ')
			{
				focus.text= focus.text.Substring(0, focus.text.Length - 1) + "_";
			}
		}
		}*/
		public override void deleteLetter()
		{
			if (!focus) { return; }
			if (focus.text.Length > 0)
			{
                        //focus.text = focus.text.Substring(0, focus.text.Length - 1);
				focus.text= focus.text.Substring(0, position-1 )+ focus.text.Substring(position,focus.text.Length - position);
				position --;

			}
    		focus.caretPosition=position ;

			updateAC();

		}
		public override void deleteWord()
		{
			if(focus.text!="")
			{
				String[] words = focus.text.Split(' ');
				int cpt=0;
				int j=0;
				while(j<words.Length && cpt < position)
				{
					cpt=cpt+words[j].Length;
                    if(j!=0) //count the space in between words
                    cpt++;
                    j++;
                }
                int toRemove;
                if(j>0)
                {
                	toRemove= words[j-1].Length;

            // if the focus character is a space, delete it and the last word
                	if(toRemove ==0)
                	{
                		deleteLetter();
                		deleteWord();
                	}else{

                		focus.text= focus.text.Substring(0, cpt-toRemove )+ focus.text.Substring(cpt,focus.text.Length-cpt );

          //  focus.text = focus.text.Substring(0, focus.text.Length  - toRemove);
                		position = cpt - toRemove;
                	}

                }
                focus.caretPosition=position ;

                updateAC();

            }
        }

        public override void clear()
        {
        	focus.text = "";
        	position =0;
           	focus.caretPosition=position ;

        	updateAC();

        }
    public void backToMenu()
    {

        print("back menu");
        //Application.LoadLevel("menu"); 
      //  SceneManager.LoadScene("menu");

        GameObject.Find("settings").GetComponent<settings>().savePreferencesSpeech(); 
    }
   
   public override void arrows(string direction){

        switch(direction) {
            case "left":
                    if(position>0)
                    {
                        position --;
                        focus.caretPosition=position ;
                        updateAC();
                    }
            break;
            case "right":
                    if(position!= focus.text.Length)
                    {
                        position ++;
                        focus.caretPosition=position ;                        
                        updateAC();
                    }
            break;
        }
   }
   public override void send(){ 
    say(focus.text);
            clear();
    }

    public override void updateAC()
    {
                ac.updateProposals(focus.text,position);
    }

    public void displayMenu()
    {
            menu.SetActive(!menu.active);
            keyboard.SetActive(!menu.active);
          focus.gameObject.SetActive(!menu.active);


    }

    public void changeVoice()
    {
        if (gender == "Maschio")
        {
            gender = "Feminile";
            voiceBtn.text = "Feminile";
        }
        else
        {
            gender = "Maschio";
            voiceBtn.text = "Maschio";
        } 
    }
    public void repeat()
    {
        print("repeat "+ lastSentence);
        say(lastSentence);
    }
}
