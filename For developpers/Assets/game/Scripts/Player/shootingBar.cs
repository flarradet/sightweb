﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Text;

using System;
using System.Collections.Generic;
using System.Linq;

public class shootingBar : MonoBehaviour {
	/*
	public Slider gunSlider;
	public Slider gsrSlider;

	public Image fillSlider;
	public Color fillSliderSave;
	public GameObject powerShootUncharged;
	*/
	Socket sock;
	public GameObject player;
	float relaxGoal, currentTime, chosenTime;
	public float gsr, colorTime,redTime,tmpTime;
	int mean,sd;
	System.Random rnd ;
    public int damagePerShot = 100; 
	// Use this for initialization
	void Start () {


		relaxGoal = 10000f;
		changeColor (0);
		currentTime = 0f;
		chosenTime = 10f;
		rnd = new System.Random ();

		mean = 18;
		sd = 4; 
		tmpTime = 0f;
		colorTime = -1;
		redTime = 2;

	}
	
	// Update is called once per frame
	void Update () {

			currentTime += Time.deltaTime;


			// shoot 
			if(chosenTime == -1)
			{
			/*	if(currentTime< 3)
				{
					GameObject.Find("powerShoot_charged").transform.localScale += new Vector3 (1f, 0.0f, 1f);
				}
				else
				{
					transform.localScale = new Vector3 (1f, 0.01f, 1f);
					//remove the weapon
					GameObject.Find("powerShoot_charged").transform.localScale = new Vector3(0.0f, 0.01f, 0.0f);


					// get next time for shooting 
					int val = rnd.Next ((mean - sd)*10, (mean + sd)*10); 
					chosenTime = val / 10f;
					changeColor (0);
					currentTime = 0f;
				}*/

					if(currentTime< 3)
				{
					transform.localScale += new Vector3 (0.1f, 0.0f, 0.1f);
				}
				else
				{
					//remove the weapon
					//GameObject.Find("powerShoot_charged").transform.localScale = new Vector3(0.0f, 0.01f, 0.0f);
					transform.localScale = new Vector3 (0.0f, 0.01f, 0.0f);


					// get next time for shooting 
					int val = rnd.Next ((mean - sd)*10, (mean + sd)*10); 
					chosenTime = val / 10f;
					changeColor (0);
					currentTime = 0f;
				}


			}
			else
			{


				// then shoot
				 if (currentTime > chosenTime) {

					//transform.localScale += new Vector3 (0.05f, 0.0f, 0.05f);
					chosenTime = -1;
					currentTime= 0;
				} 
				// first change the color
				else if ((currentTime > chosenTime - 2) ) {
					changeColor (1 - ((chosenTime - currentTime - 1) / 2));
				}
				
			}
		
	}

	void changeColor(float value)
	{

		Color altColor = Color.black;
		altColor.r = 1f;         
		altColor.g = value;        
		altColor.b = value;
		//print (gsr);
		//print (relaxGoal);
		player.GetComponent<Renderer>().material.color = altColor;
	}


	void OnTriggerEnter(Collider other)
		{
			if (other.tag == "enemy")
			{
				//print("touched enemy!");

				// Try and find an EnemyHealth script on the gameobject hit.
				 CompleteProject.EnemyHealth enemyHealth = other.GetComponent<CompleteProject.EnemyHealth>() ;
				// If the EnemyHealth component exist...
				  if (enemyHealth != null)
                   {
                       // ... the enemy should take damage.
                   	print("before"+enemyHealth.currentHealth );
                       enemyHealth.TakeDamage(damagePerShot, other.transform.position);
                       print("enemydamage");
                   	print("after"+enemyHealth.currentHealth );
                   }
			}
		}
}
