﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using ZenFulcrum.EmbeddedBrowser;

public class keyboard_manager_browser : keyboard_manager
{


    private GUIBrowserUI gb;
    public GameObject[] deleteWords;
    GameObject panel;

    [HideInInspector]
    public string writingText;
    [HideInInspector]
    int needCheckText;

    public  void Awake()
    {
             needCheckText=0;
                 writingText="";

    }
    public override void init()
    {
       isActivated = true;
        ((autoCompletion_browser)ac).enableProposals=true;
                
        gb= (GUIBrowserUI)(GameObject.Find("BrowserGUI").GetComponent<Browser>().UIHandler);
            //      for(int i=0;i< 3;i++)
              //      deleteWords[i].GetComponent<Button>().interactable= true;
   
    }
    private void OnDisable()
    {
        Time.timeScale = 1; 
        isActivated = true;
        position=0;
        ((autoCompletion_browser)ac).enableProposals=true;
        writingText="";
        position=0;
        ((autoCompletion_browser)ac).disableAll();
    //    for(int i=0;i< 3;i++)
      //      deleteWords[i].GetComponent<Button>().interactable= true;

        if(panel!=null)
        {
            panel.SetActive(true);
        }
        panel=null;

    }
    private void OnEnable()
    {

        needCheckText=10;


        if(GameObject.Find("right_panel"))
        {
           panel=  GameObject.Find("right_panel");
           panel.SetActive(false);
        }

    }
    private void Update()
    {
        if(needCheckText==1)
        {
            GameObject.Find("BrowserGUI").GetComponent<Browser>().EvalJS("document.activeElement.value").Then(res=> updateText(res)); //print(res));//
            needCheckText--;
        }
        if(needCheckText>0)
        {
            needCheckText--;
        }

    }



        public override void WriteKey(Text t)
        {
          
            gb.writeKey((char)t.text.ToCharArray()[0]);

         
                if(position == writingText.Length)
                {
                    writingText +=  t.text ;

                }
                else{
                   writingText =  writingText.Substring(0, position) + t.text + writingText.Substring( position,writingText.Length-position);
                }

            
            position++;

            updateAC();            
        }
/*
        public void WriteSpecialKey(int n)
        {
            if (n == 14 || isActivated)
            {
                float threshold;
                switch (n)
                {
                case 0: // delete letter 
                    deleteLetter();
                    break;
                case 1:  // play 
                    //say(focus.text);
                    break;
                case 2:
                    SwitchCaps();
                    break;
                case 3:
                    SetActive(false);
                    break;
                case 4:
                    SetKeyboardType(1);
                    break;
                case 5:
                    SetKeyboardType(2);
                    break;
                case 6: // previous letter 


                    gb.ExecuteKey(KeyCode.LeftArrow);
                    if(position >0)
                    {
                        position--;  
                        updateAC();
                    }
      
                    break;
                case 7: //next position

                    gb.ExecuteKey(KeyCode.UpArrow);
                    needCheckText=10;
                    break;
                case 8:
                    SetKeyboardType(0);
                    break;
                case 9: //threshold
                    threshold = GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait + 0.25f;
                    GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait = threshold;
                    GameObject.Find("thresholdText").transform.GetComponent<Text>().text = "Threshold : " + threshold;
                    break;
                case 10: // threshold
                    threshold = GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait - 0.25f;
                    threshold = Math.Max(0.25f, threshold);
                    GameObject.Find("eyeTracker").GetComponent<eye_tracker>().time_to_wait = threshold;
                    GameObject.Find("thresholdText").transform.GetComponent<Text>().text = "Threshold : " + threshold;
                    break;
                case 11: // change gender
                    if (gender == "Male")
                    {
                        gender = "Female";
                        voiceBtn.text = "Femminile";
                    }
                    else
                    {
                        gender = "Male";
                        voiceBtn.text = "Maschio";
                    }
                    break;
                case 12: // display/hide ball
                    if (GameObject.Find("ballBtn").GetComponentInChildren<Text>().text == "display ball")
                    {
                        GameObject.Find("ballBtn").GetComponentInChildren<Text>().text = "hide ball";
                        GameObject.Find("Sphere").GetComponent<Renderer>().enabled = true;
                    }
                    else
                    {
                        GameObject.Find("ballBtn").GetComponentInChildren<Text>().text = "display ball";
                        GameObject.Find("Sphere").GetComponent<Renderer>().enabled = false;
                    }
                    break;
                case 13:
                    gb.ExecuteKey(KeyCode.RightArrow);
                    if(position < writingText.Length)
                    {
                        position++;
                        updateAC();
                    }

                    break;
                case 14: // sleep mode 
                    isActivated = !isActivated;
                    for (int i=0;i< 3; i++)
                    {
                        specialKeys[i].GetComponent<Button>().GetComponent<Image>().color = buttonColors[Convert.ToInt32(!isActivated)];
                    }

                    break;
                case 15: //dissmiss keyboad and press enter key
                    gb.enterKey(1);

                    break;
                case 16: 
                    gb.ExecuteKey(KeyCode.DownArrow);
                   needCheckText=10;
                    break;
                case 17: // dissmiss keyboard 
                             gb.enterKey(0);

                    break;
                case 18: 

                    break;
                case 19:                // delete word
                        deleteWord();
                break;
                case 20: //clear
                    clear();
                break;
                case 21: //escape
                     gb.ExecuteKey(KeyCode.Escape);

                break;

            }
        }
      //  remplaceText();
    }
*/


    public override void writeWord(string text)
    {

        String focusedWord =    ac.getFocusedWord(writingText,position);

        for (int i=focusedWord.Length; i< text.Length ;i++)
        {
            gb.writeKey((char)text[i]);
        }
            gb.writeKey(' ');

        writingText= writingText.Substring(0, position - focusedWord.Length )+ text + writingText.Substring(position,writingText.Length - position)+" ";

                position= position + text.Length - focusedWord.Length+1;
                updateAC();
    }

        
        public override void deleteLetter()
        {
            if (writingText.Length > 0)
            {
                        //focus.text = focus.text.Substring(0, focus.text.Length - 1);
                writingText= writingText.Substring(0, position-1 )+ writingText.Substring(position,writingText.Length - position);
                position --;

            }

            updateAC();
            gb.ExecuteKey(KeyCode.Backspace);

        }

        
        public override void deleteWord()
        {
            
            if(writingText!="")
            {
                String[] words = writingText.Split(' ');
                int cpt=0;
                int j=0;
                while(j<words.Length && cpt < position)
                {
                    cpt=cpt+words[j].Length;
                    if(j!=0) //count the space in between words
                    cpt++;
                    j++;
                }
                int toRemove;
                if(j>0)
                {
                    toRemove= words[j-1].Length;

            // if the focus character is a space, delete it and the last word
                    if(toRemove ==0)
                    {
                        deleteLetter();
                        deleteWord();
                    }else{
   
                        writingText= writingText.Substring(0, cpt-toRemove )+ writingText.Substring(cpt,writingText.Length-cpt );  

                        for(int h=cpt-toRemove;h< position; h++)
                            gb.ExecuteKey(KeyCode.Backspace);
                        for(int h=position;h< cpt; h++)
                            gb.ExecuteKey(KeyCode.Delete);
                        
                        position = cpt - toRemove;

                    }

                }

                updateAC();
            }
        }


    public override void clear()
    {


            for(int i=0;i<position;i++)
            {
                gb.ExecuteKey(KeyCode.Backspace);
            }
            for(int i=0;i<writingText.Length;i++)
            {
                gb.ExecuteKey(KeyCode.Delete);
            }        
            writingText="";

            position=0;
            updateAC();
    }
    public void updateText(string res)
    {
        writingText = res ;      
        if(writingText==null)
        {
            writingText = "";
            //dont zoom as much as you are not sure if the window is centered properly
             gb.GetComponent<Browser>().Zoom=6;

        }
        position= writingText.Length;
        updateAC();
    }
        public override void updateAC()
    {
        ac.updateProposals(writingText,position);
    }
    public  void esc(){
        gb.ExecuteKey(KeyCode.Escape);
    }
    public  void arrows(string direction){

        switch(direction) {
            case "up":
                gb.ExecuteKey(KeyCode.UpArrow);
                needCheckText=10;
            break;
            case "down":
                gb.ExecuteKey(KeyCode.DownArrow);
                needCheckText=10;
            break;
            case "left":
                gb.ExecuteKey(KeyCode.LeftArrow);
                if(position >0)
                {
                    position--;  
                    updateAC();
                }
            break;
            case "right":
                gb.ExecuteKey(KeyCode.RightArrow);
                if(position < writingText.Length)
                {
                    position++;
                    updateAC();
                }
            break;
        }
    }

    public  void close(){ // dissmiss keyboard
        gb.enterKey(0);
    }
    public override void send(){
        //dissmiss keyboad and press enter key
        gb.enterKey(1);
    }

}
