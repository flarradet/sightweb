﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;
using UnityEngine.UI;
// Runtime code here
using System.IO;
using ZenFulcrum.EmbeddedBrowser;
using System.Runtime.InteropServices;
using howto_bounding_circle;
using System.Drawing;



public class eye_tracker_browser_v4 : eye_tracker_browser_v1
{
    public GameObject scroll;
    [DllImport("user32.dll")]
    public static extern int SetCursorPos(int x, int y);

        void Start()
    {
        mainTimer = timer;
        central= timer;
//        timer= mainTimer;

        gazePlot.SetActive(false);

        scroll = GameObject.Find("scroll");

       inizialise();
    }


          //  Util.displayCursor(true);
    public override void inizialise()
    {
        base.inizialise(); 
        Util.displayCursor(true);
    }

    public  override void desactivateButtons (bool display){
        radialPanel.SetActive(display);
        
        radialPanel.GetComponent<circularMenu>().menus[0].SetActive(false); // for some reason without these two lines the radial menu does not appearsometimes ??? 
        radialPanel.GetComponent<circularMenu>().menus[1].SetActive(true);

        radialPanel.GetComponent<circularMenu>().menus[1].SetActive(false); // for some reason without these two lines the radial menu does not appearsometimes ??? 
        radialPanel.GetComponent<circularMenu>().menus[0].SetActive(true);

        
        // activate / desactivate the menu items 
        var canvas = GameObject.Find("top");
        foreach (Transform child in canvas.transform)
        {
            if(child.gameObject.GetComponent<Button>())
                child.gameObject.GetComponent<Button>().interactable = !display;
        }
                scroll.SetActive(!display);

    }

    public void move()
    {
        print("move");
        changeReadMode();
      //  Util.displayCursor(!Util.isCursorDisplayed);

    }
    public override void changeReadMode()
    {
        readMode = !readMode ;
        scroll.SetActive(readMode);

    }
    public override void displayRadialPanel(bool display)
    {
        //          while(ShowCursor(true) <0){}
        if (displayBall)
            gazePlot.SetActive(display);
/*
        if (radialPanel.name != "right_panel")// normal menu
        {
            radialPanel.SetActive(display);
        }
        else
            desactivateAll(display);
*/
        desactivateButtons(display);


        if (display)
        {
            GameObject.Find("Read-only").GetComponent<Button>().interactable = false;
          //  Util.displayCursor(true);
        }
        else
        {
            GameObject.Find("Read-only").GetComponent<Button>().interactable = true;

/*            if (!displayBall)
            {
                Util.displayCursor(false);
            }*/
        }
        resetPointsBrowser();

    }
/*
    public override void mouseGoToPosition(Vector2 pos)
    {
        if(!readMode)
        base.mouseGoToPosition(pos);
    }
*/

        // move the radial menu where the mouse is unless there is no space then move it a little 
    public override void radialGoToCenter(Vector2 center)
    {
        print("radialgotocenter");
            RaycastHit hitRadial;
            if (center.y < 250)
                center.y = center.y + 270;
            else if(center.y >Screen.height - 400)
            {
                center.y = center.y - 270;
            }
            if (center.x < 250 )
                center.x = center.x + 270;
            else if (center.x > Screen.width - 250)
                center.x = center.x - 270  ;

            wait = 5;

            if (Physics.Raycast(cameraGUI.ScreenPointToRay(new Vector2(center.x, center.y)), out hitRadial, 100000000f, LayerMask.GetMask("radial")))
            {
                radialPanel.transform.position = hitRadial.point;
            }
    }


}