﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Tobii.EyeTracking;
using Tobii.Gaming;
using UnityEngine.UI;
// Runtime code here
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;
using ZenFulcrum.EmbeddedBrowser;
using System.Runtime.InteropServices;
using howto_bounding_circle;
using System.Drawing;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class eye_tracker_browser : Eye_tracker_interface
{

    // gameobjects linked in GUI
    public GameObject keyboard;
    public GameObject radialPanel;
    public GameObject[] settings;

    ///////////////

    [HideInInspector]
    public float time_to_wait_menu, time_to_wait_radialButtons, start_time, radius_menu;
    [HideInInspector]
    public bool readMode;
    [HideInInspector]
    public PointF center;

    private bool browserLookAtField = false;
    private string nameBrowserInput;
    private bool full;
    private Vector3 savePosMouse;
    private bool done = false;
    [HideInInspector]
    public int wait = 0;

    public Ray camRayBrowser, camRayGaze;
    RaycastHit  hitBrowser;
    Vector3[] smoothedPoints;


    [DllImport("user32.dll")]
    public static extern int SetCursorPos(int x, int y);
    [DllImport("user32.dll")]
    public static extern bool GetCursorPos(out Point lpPoint);

    [HideInInspector]
    public List<System.Drawing.PointF> lookingPoints;
    // DO NOT PUT ANYTHING BEHING THIS LINE, IT WONT BE TAKEN INTO ACCCOUNT IN STANDALONE ( BUG UNITY?? )


    public virtual void inizialise()
    {
        full = false;
        sleepMode = false;
        readMode = true;
        setup();
        start_time = Time.time;
        displayRadialPanel(false);   

    }

    public void updateFunction()
    {

        // RADIAL AUTO POSITION 
        // if we have updated the mouseposition through the radial menu, update also the radial menu position ( cannot be done right after updating the mouse position because Input.mousePosition is not updated yet )
        /**/
        if (isActive() && Input.mousePosition != savePosMouse)
        {
            radialGoToCenter(Input.mousePosition);
            savePosMouse = Input.mousePosition;
        }
        //*/

        gazeAction();
}


    public void changeBrowserInput(string nameCursor)
    {
        browserLookAtField = true;
        nameBrowserInput = nameCursor;

    }
    public void resetBrowserInput()
    {
        browserLookAtField = false;
    }

    public void mouseUpdatePosition(string coordonates)
    {
        int x = int.Parse(coordonates.Split(';')[0]);        
        int y = int.Parse(coordonates.Split(';')[1]);        

        Point p;
        GetCursorPos(out p);
        Vector3 avant = Input.mousePosition;
        Vector2 newPos = new Vector2();
        // while(Input.mousePosition == avant)
        SetCursorPos(p.X + x, p.Y + y);
        //print(Input.mousePosition);

        //  GetCursorPos(out p);
        //   radialGoToCenter( new Vector2(p.X,p.Y));

    }
    public virtual void mouseGoToPosition(Vector2 pos)
    {
        // move the real mouse
        Vector3 screenPos = cameraGUI.WorldToScreenPoint(pos);
#if UNITY_EDITOR

        System.Reflection.Assembly assembly = typeof(UnityEditor.EditorWindow).Assembly;
        System.Type type = assembly.GetType("UnityEditor.GameView");
        EditorWindow gameview  = EditorWindow.GetWindow(type);
        SetCursorPos((int)(screenPos.x+gameview.position.x),(int)(-screenPos.y+gameview.position.y +gameview.position.height ));
#else
        SetCursorPos((int)(screenPos.x), (int)(-screenPos.y + Screen.height));
#endif

    }

    public void resetPointsBrowser()
    {
        lookingPoints = new List<PointF>();
        full = false;
        start_time = Time.time;
    }

    public virtual bool isActive(){ return false;}
/*

    bool isActive()
    {
        if (radialPanel.name != "right_panel")// normal menu
        {
            return radialPanel.active;
        }
        else
            return radialPanel.GetComponent<CanvasGroup>().interactable;
    }
    */
    /*
    void desactivateAll(bool display)
    {
        radialPanel.GetComponent<CanvasGroup>().interactable = display;
        var canvas = radialPanel;
        foreach (Transform child in canvas.transform)
        {
            foreach (Transform button in child.transform)
            {
                button.gameObject.GetComponent<Button>().interactable = display;
            }
        }

    }*/
    public  virtual void desactivateButtons (bool display){}

    public virtual void displayRadialPanel(bool display)
    {
        //          while(ShowCursor(true) <0){}
        if (displayBall)
            gazePlot.SetActive(display);
/*
        if (radialPanel.name != "right_panel")// normal menu
        {
            radialPanel.SetActive(display);
        }
        else
            desactivateAll(display);
*/
        desactivateButtons(display);


        if (display)
        {
            GameObject.Find("Read-only").GetComponent<Button>().interactable = false;
            Util.displayCursor(true);
        }
        else
        {
            GameObject.Find("Read-only").GetComponent<Button>().interactable = true;

            if (!displayBall)
            {
                Util.displayCursor(false);
            }
        }
        resetPointsBrowser();

    }

    public virtual void radialGoToCenter(Vector2 center){ }

    public override void getDataFromPlayerPrefs()
    {

        // restore saved data preferences 

        if (GameObject.Find("settings"))
        {
            settings set = GameObject.Find("settings").GetComponent<settings>();

            time_to_wait_main = set.sp.browser.time_to_wait_main; //2.0f
            time_to_wait_menu = set.sp.browser.time_to_wait_menu;  //1.5f;
            displayBall = set.sp.browser.displayBall;//true;

            radius_menu = set.sp.browser.radius_menu; //30f;
            set.browserObject = GameObject.Find("BrowserGUI");

            time_to_wait_radialButtons = set.sp.browser.time_to_wait_radialButtons;  //1.5f;

        }
        else  // if we run the browser directly, not throught the whole system 
        {
            time_to_wait_main = 1.0f;
            time_to_wait_menu = 1.0f;//2.75f;
            time_to_wait_radialButtons = 1.0f;
            displayBall = false;
            radius_menu = 15f;
        }


        // update the menu texts according to the player preferences
        settings[0].GetComponent<Text>().text = settings[0].GetComponent<Text>().text + time_to_wait_main;
        settings[1].GetComponent<Text>().text = settings[1].GetComponent<Text>().text + time_to_wait_menu;


        settings[2].GetComponent<Text>().text = settings[2].GetComponent<Text>().text + radius_menu;
        settings[3].GetComponent<Text>().text = settings[3].GetComponent<Text>().text + time_to_wait_radialButtons;
        
        settings[4].GetComponent<MyButton>().setIsActivated(displayBall);

        if (displayBall)
        {
            Util.displayCursor(true);
        }
        else
        {
            Util.displayCursor(false);
        }
    }
    /*****************************
    //
    // functions for  gazeAction 
    //
    //
    *****************************/


    // the normal gaze needs to be quick in order to go from one key to another so smooth it but not too much, for the browser, smooth according to the distance, so go quick to go from one side of the screen to the otehr side but smooth a lot when its small distances to avoid jittering 
       public override void getFinalPoint()
    {

        Vector3 fixedPoint = Util.calculateFixedPoint(gazePosition);

        if(keyboard.active)
            smoothedPoints = Util.Smoothify3(fixedPoint, 0.8f);
        else
            smoothedPoints =Util.Smoothify3(fixedPoint, 0.9f);

        camRayBrowser = cameraGUI.ScreenPointToRay(smoothedPoints[0]);
        camRayGaze = cameraGUI.ScreenPointToRay(smoothedPoints[1]);
    }


    // move and display the ball/ browser cursor if necessary. if they look at a fixed point open the menu
    public override void ActionsWhenPointValid()
    {


        // if the gaze is within the browser and we are not in the radial menu and it is not read mode
        if (Physics.Raycast(camRayBrowser, out hitBrowser, float.PositiveInfinity, LayerMask.GetMask("browser")) && !isActive() && !readMode && !sleepMode)
        {

            //     print("gp"+gazePosition);
            if (wait == 0  )
            {
                  if(!readMode)
                        mouseGoToPosition(smoothedPoints[0]);
            }
            else
                wait = wait - 1;

            gazePlot.SetActive(false);

        }
        else if (!sleepMode) // move the gaze ball
        {
            if (Physics.Raycast(camRayGaze, out hit, float.PositiveInfinity, LayerMask.GetMask("Background")))
            {

                if (displayBall)
                    gazePlot.SetActive(true);

                //  gazePlot.transform.position = Smoothify3(hit.point,0.95f);
                gazePlot.transform.position = hit.point;
                //EditorApplication.isPaused= true;
            }
        }

        // if the radial menu is not active, activate it when the user focus on a point

        if (!isActive() && !readMode && !sleepMode)
        {

            // if we look at the browser
            if (Physics.Raycast(camRayBrowser, out hit, float.PositiveInfinity, LayerMask.GetMask("browser")))
            {

                gazePosition = new Vector3(hit.point.x, hit.point.y, 0.0f);

                // first get gaze points during the threshold display menu time 
                if (!full && Time.time - start_time < time_to_wait_menu)
                {
                    // lookingPoints.Add(new PointF(_historicPoint_browser.x,_historicPoint_browser.y));
                    lookingPoints.Add(new PointF(smoothedPoints[0].x, smoothedPoints[0].y));
                }
                else if (!full && lookingPoints.Count !=0)
                {// we finished completing the vector of points
                    full = true;
                }
                else if(!full && lookingPoints.Count ==0)
                {
                    start_time= Time.time;
                }
                else // if we have all the points for the number of second necessary
                {
                    // check whether or not  the points are withing a small distance 
                    float radius;
                    
                   // for(int k=0; k < lookingPoints.Count; k++)
                     //   print(lookingPoints[k]);
                    
                    Geometry.FindMinimalBoundingCircle(lookingPoints, out center, out radius);
                    
                    // if yes , display the point and display the radial menu
                    Vector2 centerPos = new Vector2(center.X, center.Y);

                    if (radius < radius_menu && GameObject.Find("BrowserGUI").GetComponent<Browser>().IsLoaded)
                    {

                        displayRadialPanel(true);

                        radialGoToCenter(centerPos);
                        resetPointsBrowser();
                    }
                    else
                    {
                        // remove first point
                        lookingPoints.RemoveAt(0);
                        // add new point 
                        lookingPoints.Add(new PointF(Util._historicPoint_browser.x, Util._historicPoint_browser.y));
                    }
                }
            }
            else // if we stop watching the browser, reset the points
            {
                resetPointsBrowser();

            }
        }
    }

    // calculate how long to wait for the button to be activated 
    public override void getTime_to_wait()
    {
        // if we have the menu open, wait for the menu time 
        if (isActive())
            time_to_wait = time_to_wait_radialButtons;
        else if (newName.Length >= 5)
        {
            // if it is a auto completion proposition or a threshold button or the center of the radial menu , wait longuer than normal
            if (newName.Substring(0, 5) == "propo" )
                time_to_wait = time_to_wait_main+1;
            else if (newName.Substring(0, 5) == "thres" )
                time_to_wait = Mathf.Max(time_to_wait_main,1);
            else if ( newName.Substring(0, 5) == "sleep")
                time_to_wait = Mathf.Max(time_to_wait_main,5);
            else
                time_to_wait = time_to_wait_main;
        }
        else
        {
            time_to_wait = time_to_wait_main;
        }

        // if this is the center of the radial menu, wait a little more so that people dont click it by inadvertance
        if (newName == "4" && SceneManager.GetActiveScene().name == "browser_v1")
        {
            time_to_wait = time_to_wait + 1;
        }
        // if we are moving the mouse or scrolling the browser and this key has already been selected, do it quicker next time ( if not already super small)
        if ((oldname == newName) && (GameObject.Find("titlesScroll") || GameObject.Find("titlesMouse") || newName == "top" || newName == "bottom" || newName == "right" || newName == "left"))
        {
            time_to_wait = Mathf.Min(1f,time_to_wait_radialButtons);
        }
        if ((oldname == newName) && newName == "Read-only" )
        {
            time_to_wait = Mathf.Max(3f,time_to_wait);
        }
    }

    public override bool touchSomething(string mask)
    {
        return Physics.Raycast(camRayGaze, out hit, float.PositiveInfinity, LayerMask.GetMask(mask));
    }
    /*
    // do things if we look at something on UI 
    public override bool touchSomething(string mask)
    {
        return Physics.Raycast(camRayGaze, out hit, float.PositiveInfinity, LayerMask.GetMask(mask)) || (isActive() && radialPanel.GetComponent<circularMenu>());
    }*/
    /*
    public override void getNewName()
    {
        if (isActive() && radialPanel.GetComponent<circularMenu>())
        {
            newName = radialPanel.GetComponent<circularMenu>().selectionName;
            if (newName == "-1")
            {
                lookNothing();
            }
        }
        else
        {
            newName = hit.collider.gameObject.name;
        }
    }*/
        public override void getNewName()
        {
            newName = hit.collider.gameObject.name;
        }

/*
    public override void doActionButton()
    {

        if ((newName == "sleep" || !sleepMode) && timer.enabled)
        {
            // if we are selecting a key from the browser menu
            if (isActive() && newName != "" && radialPanel.GetComponent<circularMenu>())
            {
                // reset the mouse position to aknowledge the position by the browser
                radialPanel.GetComponent<circularMenu>().executeAction(oldname, new Vector2(center.X, center.Y));
                resetPointsBrowser();
            } // if it is a menu key or a keyboard key execute the action on the key
            else if (!isActive() || !radialPanel.GetComponent<circularMenu>())
            {
                hit.collider.gameObject.GetComponent<Button>().onClick.Invoke(); // execute the key 

                // briefly paint the key in green to aknowledge it  
                saveColorImage = hit.collider.gameObject.GetComponent<Button>().GetComponent<UnityEngine.UI.Image>().color;
                hit.collider.gameObject.GetComponent<Button>().GetComponent<UnityEngine.UI.Image>().color = new UnityEngine.Color(0.2F, 0.72F, 0.32F);//new Color(0.22F, 0.22F, 0.22F);
                lastImage = hit.collider.gameObject.GetComponent<Button>().GetComponent<UnityEngine.UI.Image>();

            }
        }
    }
*/

            public override void doActionButton()
        {
        if ((newName == "sleep" || !sleepMode) && timer.enabled)
        {

                hit.collider.gameObject.GetComponent<Button>().onClick.Invoke(); // execute the key 

                // briefly paint the key in green to aknowledge it  
                //lastImage =  hoverImage; //new MyButton(hit.collider.gameObject.GetComponent<Button>().GetComponent<UnityEngine.UI.Image>());
                //lastImage.selected();
                lastImage =  hit.collider.gameObject.GetComponent<MyButton>();
                lastImage.setStatus(Util.ButtonStatus.Pressed);

        }    
    }

    public override void getTime_ignore()
    {

        time_ignore = time_ignore_main;
        if (name2 == oldname)
        {
            time_ignore = time_ignore_main + 0.2f;
        }


        if (newName == "4")
        {
            time_ignore = time_ignore + 1;
        }

    }
    public virtual void displayTimer(){  timer.enabled = true; }
    public override void startConsideringKey()
    {
        if (newName == "sleep" || !sleepMode)
        {
            displayTimer();
        }
  

        if (hit.collider && !hit.collider.gameObject.GetComponent<Button>().interactable)
        {
            timer.enabled = false;
        }
        moveTimerToPosition();
    }
    // move the position to the displayed timer to this new key                         
    public virtual void moveTimerToPosition()
    {
        // transform.position depend of the center of the collider which is not the same for each key because of the anchors
         if (hit.collider.gameObject.name.Substring(0, 3) == "Key" || hit.collider.gameObject.name.Substring(0, 3) == "pro")
        {
            timer.transform.position = hit.collider.gameObject.transform.position;
        }
        else
        {
            timer.transform.position = new Vector3(hit.collider.gameObject.transform.position.x + 3.5f, hit.collider.gameObject.transform.position.y - 3.5f, hit.collider.gameObject.transform.position.z);
        }
    }
    public virtual void changeReadMode()
    {
        readMode = !readMode ;

    }

    public void changeSleepMode()
    {
        sleepMode = !sleepMode ;
    }


    public void setThreshold(string datas)
    {
        int index= int.Parse(datas.Split(';')[0]);
        float val=float.Parse(datas.Split(';')[1]);

         if(index==0)
            thresholdModify(ref time_to_wait_main,settings[index].GetComponent<Text>(),val);     
        else if(index==1)            
            thresholdModify(ref time_to_wait_menu,settings[index].GetComponent<Text>(),val);     
        else if(index==2)
            thresholdModify(ref radius_menu,settings[index].GetComponent<Text>(),val);     
        else if(index==3)
            thresholdModify(ref time_to_wait_radialButtons,settings[index].GetComponent<Text>(),val);     
    }

    public void setDisplayBall()
    {
        displayBall = !displayBall ;
        gazePlot.SetActive(displayBall);
        Util.displayCursor(!Util.isCursorDisplayed);
    }


}
