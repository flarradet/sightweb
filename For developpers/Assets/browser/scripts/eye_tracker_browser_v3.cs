﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Tobii.EyeTracking;
using Tobii.Gaming;
using UnityEngine.UI;
// Runtime code here
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;
using ZenFulcrum.EmbeddedBrowser;
using System.Runtime.InteropServices;
using howto_bounding_circle;
using System.Drawing;

public class eye_tracker_browser_v3 : eye_tracker_browser
{

    void Start()
    {
                readMode= false;

        inizialise();

        displayRadialPanel(true);

    }

    void Update()
    {
        updateFunction();

    }

    public override bool isActive()
    {

            return radialPanel.GetComponent<CanvasGroup>().interactable;
    }




    public  override void desactivateButtons (bool display){
        print("desactivateall"+display);
        radialPanel.GetComponent<CanvasGroup>().interactable = display;
        var canvas = radialPanel;
        foreach (Transform child in canvas.transform)
        {
            foreach (Transform button in child.transform)
            {
                button.gameObject.GetComponent<Button>().interactable = display;
            }
        }
                            readMode = !display;
        GameObject.Find("Read-only").GetComponent<MyButton>().setIsActivated(true);
    }
}