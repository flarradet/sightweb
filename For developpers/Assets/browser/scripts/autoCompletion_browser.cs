﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine.UI;

public class autoCompletion_browser : autoCompletion {


    public bool enableProposals=true;

    // Use this for initialization
    void Start () {

        import();   
    }
    public override void updateProposals(string text,int position)
    {
    
        if(enableProposals)
        {
       // text= text;
        if (text == "")
        {
            proposals[0].text = "non";
            proposals[1].text = "di";
            proposals[2].text = "che";
            proposals[3].text = "è";
            proposals[4].text = "e";
            proposals[5].text = "la";
                        // if its the letter keyboard that is activated 
            if(GameObject.Find("keyboard").GetComponent<keyboard_manager_browser>().panels[0].activeSelf)
            {
                checkPossibleLetters("");
            }   
        }
        else if(position == text.Length || text[position]==' ' )
            {
               String focusedWord = getFocusedWord(text,position);
             // StringBuilder result = new StringBuilder(2000);

                import();
               // String lastWord = words[words.Length - 1];
              //  getResults(focusedWord, result);
                string result= getRes(focusedWord);

                string tmp = result.ToString().Split('[')[2];
                string[] res = tmp.Split('"');
                int i = 0;

                while (i * 2 + 1 < res.Length &&  i< 6)
                {
                    proposals[i].text = res[i * 2 + 1];
                    proposals[i].transform.parent.GetComponent<Button>().interactable =true;
                    i++;

                }
                while(i<6)
                {
                    proposals[i].text = "";
                    proposals[i].transform.parent.GetComponent<Button>().interactable =false;
                    i++;
                }

            }
        else
        {
            disableAll();
        }
    }
    }



public void disableAll()
{
                    for(int i=0;i<6; i++)
                {
                    proposals[i].text = "";
                    proposals[i].transform.parent.GetComponent<Button>().interactable =false;
                }  
}

}