﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tobii.Gaming;
using ZenFulcrum.EmbeddedBrowser;
using UnityEngine.SceneManagement;

public class circularMenu_v4 : circularMenu {



	public override void GetCurrentMenuItem()
	{
			Vector2 centerCanvas = GameObject.Find("radial").transform.position;

	
    		Vector3 gazePosition=Util._historicPoint_gaze;
    		/**/
    		/////////
    		//////
    		//use the radial menu with the eye tracker 
			/////
			///////
			Vector3 mousePos= new Vector3(0,0,0);
			RaycastHit hit;
	    		if (Physics.Raycast(overGUI.ScreenPointToRay(gazePosition), out hit, 100000000f, LayerMask.GetMask("radial")))
	    		{
	               mousePos = hit.point;//Smoothify3(hit.point);
	            }
	         //*/

    		/*
    		//////////
    		////
    		// use the radial menu with the mouse position instead of the eye trakcer position ( you will need to desactivate the radial auto reposition in the eye_tracker browser )
    		////
    		////////
			Vector2 Mouseposition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
			 gazePosition= Mouseposition;
			Vector3 mousePos= Mouseposition;

	            	RaycastHit hit;
	    		if (Physics.Raycast(Camera.main.ScreenPointToRay(gazePosition), out hit, 100000000f, LayerMask.GetMask("radial")))
	    		{
	               mousePos = hit.point;//Smoothify3(hit.point);
	 			}
	 			//*/
			



	       	float angle = Mathf.Atan2(mousePos.y-centerCanvas.y,mousePos.x-centerCanvas.x) * (180 / Mathf.PI);
	      /* 	3 items 

	      if(angle > 90)
		       	angle = 360 -( angle-90);
		    else
		       	angle= -(angle-90);
		       	print(angle);*/

			angle= (135-angle);
			if(angle < 0)
				angle = 315+(45+angle);
			float distance = Mathf.Sqrt(Mathf.Pow((mousePos.x- centerCanvas.x),2)+ Mathf.Pow((mousePos.y-centerCanvas.y),2));
			print("distance"+ distance);
				// dont count the middle item
				CurMenuItem =(int)(angle/(360/ (4)));
				if(distance <6)
				{
					CurMenuItem = -1;
				}
				else if(distance < 16) // if click/ close menu
				{
					if(angle > 45 && angle < 225)
					{
						CurMenuItem = 4;
					}
					else
					{
						CurMenuItem = 5;
					}

				}

				selectionName=CurMenuItem.ToString();


				if(CurMenuItem != OldMenuItem )
				{

					if( OldMenuItem != -1)
					{
						Color c;
						ColorUtility.TryParseHtmlString( "CACACAFF", out  c); 
						buttons[OldMenuItem].sceneimage.color = c;
						
						//buttons[OldMenuItem].sceneimage.color = buttons[OldMenuItem].NormalColor;
					}
					OldMenuItem = CurMenuItem;

					if( CurMenuItem != -1)
					{
						buttons[CurMenuItem].sceneimage.color = Color.red;// buttons[CurMenuItem].HighlightedColor;
					}
				}
		
	}

	public override Vector3 getCenterPoint(string point)
	{
		int angle = 0;
		int radius = 20;
		if( point =="4" || point == "5")
			radius=10;
		 if (point =="0")
			angle = -30; // 120;
		else if (point == "1" || point == "4" )
			angle = 0;//-120;
		else if (point == "2")
			angle = 30; //10;
		else if (point == "3" || point == "5")
			angle = -22; //-10;
	     return   new Vector3( transform.position.x + (radius * Mathf.Cos(angle)),  transform.position.y + (radius * Mathf.Sin(angle)), transform.position.z);
		

	}

public  override void executeAction(string name, Vector2 pos )
{


				// mouve the mouse
				// mouve the radial 
				// be careful of edges 	
		if (name=="5")// click
		{

	   		GameObject.Find("BrowserGUI").GetComponent<GUIBrowserUI>().click();
			GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().displayRadialPanel(false);
				GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().changeReadMode();
	        GameObject.Find("Read-only").GetComponent<MyButton>().setIsActivated(false);

		}
			else if (name=="4")// close
		{

				GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().changeReadMode();
			    GameObject.Find("Read-only").GetComponent<MyButton>().setIsActivated(false);

				GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser_v4>().displayRadialPanel(false);
			
		}
		else
		{
			string coordonates;
			if(name =="0")
				coordonates = "0;-10";
			else if(name =="1")
				coordonates = "10;0";
			else if(name =="2")
				coordonates = "0;10";
			else 
				coordonates = "-10;0";
			GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().mouseUpdatePosition(coordonates);

		}
}




}





