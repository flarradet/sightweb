﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Tobii.EyeTracking;
using Tobii.Gaming;
using UnityEngine.UI;
// Runtime code here
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;
using ZenFulcrum.EmbeddedBrowser;
using System.Runtime.InteropServices;
using howto_bounding_circle;
using System.Drawing;

public class eye_tracker_browser_v2 : eye_tracker_browser
{

    void Start()
    {
                GameObject.Find("Read-only").GetComponent<MyButton>().setIsActivated(true);

        inizialise();

    }

    void Update()
    {
        updateFunction();

    }

    // move the radial menu where the mouse is unless there is no space then move it a little 
    public override void radialGoToCenter(Vector2 center)
    {

            if (center.x < Screen.width / 2)
                radialPanel.transform.localPosition = new Vector3(0, 20, 0);
            else
                radialPanel.transform.localPosition = new Vector3(-1200, 20, 0);
            wait = 5;
    }

    public override bool isActive()
    {
            return radialPanel.active;
    }




    public  override void desactivateButtons (bool display){
            radialPanel.SetActive(display);
                    readMode = !display;
        GameObject.Find("Read-only").GetComponent<MyButton>().setIsActivated(true);
    }
}