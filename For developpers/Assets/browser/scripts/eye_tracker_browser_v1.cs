﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tobii.Gaming;
using UnityEngine.UI;
// Runtime code here
using System.IO;
using ZenFulcrum.EmbeddedBrowser;
using System.Runtime.InteropServices;
using howto_bounding_circle;
using System.Drawing;



public class eye_tracker_browser_v1 : eye_tracker_browser
{
    public UnityEngine.UI.Image mainTimer, central;

    void Start()
    {
        mainTimer = timer;
        //        central = timer;

        radialPanel.SetActive(true);
      //  print( GameObject.Find("Middle"));
        central = GameObject.Find("Middle").GetComponent<UnityEngine.UI.Image>();
     radialPanel.SetActive(false);
        

        GameObject.Find("Read-only").GetComponent<MyButton>().setIsActivated(true);
//        timer= mainTimer;
       inizialise();
    }

    void Update()
    {
      //  print("eye tracker update");
       updateFunction();

    }

    // move the radial menu where the mouse is unless there is no space then move it a little 
    public override void radialGoToCenter(Vector2 center)
    {
        print("radialgotocenter");
            RaycastHit hitRadial;
            if (center.y < 200)
                center.y = center.y + 220;

            if (center.x < 200)
                center.x = center.x + 200;
            else if (center.x > Screen.width - 200)
                center.x = center.x - 200;

            wait = 5;

            if (Physics.Raycast(cameraGUI.ScreenPointToRay(new Vector2(center.x, center.y)), out hitRadial, 100000000f, LayerMask.GetMask("radial")))
            {
                radialPanel.transform.position = hitRadial.point;
            }
    }


    // do things if we look at something on UI 
    public override bool touchSomething(string mask)
    {
        return Physics.Raycast(camRayGaze, out hit, float.PositiveInfinity, LayerMask.GetMask(mask)) || (isActive());
    }

    public override bool isActive()
    {
           return radialPanel.active;
    }

    public override void getNewName()
    {
        if (isActive() )
        {
            newName = radialPanel.GetComponent<circularMenu>().selectionName;
            if (newName == "-1")
            {
                lookNothing();
            }
        }
        else
        {
            newName = hit.collider.gameObject.name;
        }
    }
        public override void doActionButton()
    {

        if ((newName == "sleep" || !sleepMode) && timer.enabled)
        {
            // if we are selecting a key from the browser menu
            if (isActive() && newName != "" )
            {
                // reset the mouse position to aknowledge the position by the browser
                radialPanel.GetComponent<circularMenu>().executeAction(oldname, new Vector2(center.X, center.Y));
                resetPointsBrowser();
            } // if it is a menu key or a keyboard key execute the action on the key
            else if (!isActive())
            {
                hit.collider.gameObject.GetComponent<Button>().onClick.Invoke(); // execute the key 

                // briefly paint the key in green to aknowledge it  
              //  lastImage= hoverImage;//new MyButton(hit.collider.gameObject.GetComponent<Button>().GetComponent<UnityEngine.UI.Image>());
               // lastImage.selected();
                lastImage= hit.collider.gameObject.GetComponent<MyButton>();
                lastImage.setStatus(Util.ButtonStatus.Pressed);
            }
        }
    }
    
    // move the position to the displayed timer to this new key                         
        public override void moveTimerToPosition()
    {
        if (isActive())
        { // if it is a browser radial menu, set it to the middle of the menu button
          
            timer.transform.position = radialPanel.GetComponent<circularMenu>().getCenterPoint(newName);
        }  // transform.position depend of the center of the collider which is not the same for each key because of the anchors
        else if (hit.collider.gameObject.name.Substring(0, 3) == "Key" || hit.collider.gameObject.name.Substring(0, 3) == "pro")
        {
            timer.transform.position = hit.collider.gameObject.transform.position;
        } 
        else
        {
            timer.transform.position = new Vector3(hit.collider.gameObject.transform.position.x + 3.5f, hit.collider.gameObject.transform.position.y - 3.5f, hit.collider.gameObject.transform.position.z);
        }
    }

    public  override void desactivateButtons (bool display){
        radialPanel.SetActive(display);
        
            radialPanel.GetComponent<circularMenu>().menus[0].SetActive(false); // for some reason without these two lines the radial menu does not appearsometimes ??? 
            radialPanel.GetComponent<circularMenu>().menus[1].SetActive(true);
        
            radialPanel.GetComponent<circularMenu>().menus[0].SetActive(true); // close all titles and display the main one
            radialPanel.GetComponent<circularMenu>().menus[1].SetActive(false);
            radialPanel.GetComponent<circularMenu>().menus[2].SetActive(false);
            radialPanel.GetComponent<circularMenu>().menus[3].SetActive(false);
        
        // activate / desactivate the menu items 
        var canvas = GameObject.Find("top");
        foreach (Transform child in canvas.transform)
        {
            if(child.gameObject.GetComponent<Button>())
                child.gameObject.GetComponent<Button>().interactable = !display;
        }

        readMode = !display;
        GameObject.Find("Read-only").GetComponent<MyButton>().setIsActivated(true);
    }
    public override void displayTimer()
    {
        
      

            if (newName != "4") //  if middle button radial menu dont display the timer , use the button itself
            {
                central.enabled = false;
                mainTimer.enabled = true;
                timer = mainTimer;
            }
            else
            {

                mainTimer.enabled = false;
                central.enabled = true;
                timer = central;
            }
    }



}