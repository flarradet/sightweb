﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Tobii.Gaming;
using ZenFulcrum.EmbeddedBrowser;
using UnityEngine.SceneManagement;

public class circularMenu : MonoBehaviour {

	public List<MenuButton> buttons = new List<MenuButton>();
	private Vector2 Mouseposition;
	private Vector3 mousePos =new Vector3(0,0,0);

	public int menuItems;
	public int CurMenuItem;
	public int OldMenuItem;
	public Camera overGUI;
	public string selectionName;
	public GameObject[] menus;
	Vector2[] offsets;
	// Use this for initialization
	void Start () {
	
		menuItems = buttons.Count;
		foreach(MenuButton button in buttons)
		{
			//button.sceneimage.color =  button.NormalColor;
				Color c;
					ColorUtility.TryParseHtmlString( "CACACAFF", out  c); 
					button.sceneimage.color  = c;
		}
		CurMenuItem = 0;
		OldMenuItem = 0;


	}
	
	// Update is called once per frame
	void Update () {
		GetCurrentMenuItem();
	}



	

	public virtual void GetCurrentMenuItem()
	{
			Vector2 centerCanvas = GameObject.Find("radial").transform.position;

	
    		Vector3 gazePosition=Util._historicPoint_gaze;
    		/**/
    		/////////
    		//////
    		//use the radial menu with the eye tracker 
			/////
			///////
			Vector3 mousePos = new Vector3(0,0,0);
			RaycastHit hit;
	    		if (Physics.Raycast(overGUI.ScreenPointToRay(gazePosition), out hit, 100000000f, LayerMask.GetMask("radial")))
	    		{
	               mousePos = hit.point;//Smoothify3(hit.point);
	            }
	         //*/

    		/*
    		//////////
    		////
    		// use the radial menu with the mouse position instead of the eye trakcer position ( you will need to desactivate the radial auto reposition in the eye_tracker browser )
    		////
    		////////
			Mouseposition = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
			 gazePosition= Mouseposition;
			Vector3 mousePos= Mouseposition;

	            	RaycastHit hit;
	    		if (Physics.Raycast(Camera.main.ScreenPointToRay(gazePosition), out hit, 100000000f, LayerMask.GetMask("radial")))
	    		{
	               mousePos = hit.point;//Smoothify3(hit.point);
	 			}
	 			//*/
			



	       	float angle = Mathf.Atan2(mousePos.y-centerCanvas.y,mousePos.x-centerCanvas.x) * (180 / Mathf.PI);
	      /* 	3 items 

	      if(angle > 90)
		       	angle = 360 -( angle-90);
		    else
		       	angle= -(angle-90);
		       	print(angle);*/

			angle= (135-angle);
			if(angle < 0)
				angle = 315+(45+angle);
			float distance = Mathf.Sqrt(Mathf.Pow((mousePos.x- centerCanvas.x),2)+ Mathf.Pow((mousePos.y-centerCanvas.y),2));

				// dont count the middle item
				CurMenuItem =(int)(angle/(360/ (buttons.Count-1)));
				if(distance <12)
				{
					CurMenuItem = -1;
				}
				if(distance <7)
				{
					CurMenuItem = 4;

				}


				selectionName=CurMenuItem.ToString();


				if(CurMenuItem != OldMenuItem && CurMenuItem != -1)
				{
					
										Color c;
					ColorUtility.TryParseHtmlString( "CACACAFF", out  c); 
					buttons[OldMenuItem].sceneimage.color = c;
					
					//buttons[OldMenuItem].sceneimage.color = buttons[OldMenuItem].NormalColor;
					OldMenuItem = CurMenuItem;
					buttons[CurMenuItem].sceneimage.color = Color.red;// buttons[CurMenuItem].HighlightedColor;
				}
		
	}

	public virtual  Vector3 getCenterPoint(string point)
	{
		int angle = 0;
		int radius = 17;
		if( point =="4")
			radius=0;
		else if (point =="0")
			angle = -30; // 120;
		else if (point == "1")
			angle = 0;//-120;
		else if (point == "2")
			angle = 30; //10;
		else if (point == "3")
			angle = -22; //-10;
	     return   new Vector3( transform.position.x + (radius * Mathf.Cos(angle)),  transform.position.y + (radius * Mathf.Sin(angle)), transform.position.z);
		

	}

public virtual void executeAction(string name, Vector2 pos )
{
	if(GameObject.Find("titlesMenu"))
	{
		//scroll menu
		if( name == "0") 
		{
			//GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().readMode = true;

			menus[0].SetActive(false);
			menus[1].SetActive(true);
			//gameObject.transform.position= new Vector3(gameObject.transform.position.x+20,gameObject.transform.position.y,gameObject.transform.position.z);

		} // move mouse menu
		else if( name == "1")
		{
			menus[0].SetActive(false);
			menus[2].SetActive(true);	

		}else if( name == "2") // close
		{
			GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().displayRadialPanel(false);

		}	else if( name == "3") // zoom menu
		{
						//GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().readMode = true;

			menus[0].SetActive(false);
			menus[3].SetActive(true);	
		}
		else if( name == "4") //click
		{
	   		GameObject.Find("BrowserGUI").GetComponent<GUIBrowserUI>().click();
			GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().displayRadialPanel(false);
		}

	}else if(GameObject.Find("titlesScroll")) // if this is the scroll menu
	{
		//close the menu
		if( name == "4")
		{
			GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().displayRadialPanel(false);
		} // scroll
		else 
			GameObject.Find("BrowserGUI").GetComponent<GUIBrowserUI>().scroll(name);
	}
	else if(GameObject.Find("titlesZoom")) // if this is the zoom menu 
	{
		if( name == "3") // z-
		{
			GameObject.Find("BrowserGUI").GetComponent<GUIBrowserUI>().updateZoom(-1);
		} // z+
		else if( name == "1")
		{
			GameObject.Find("BrowserGUI").GetComponent<GUIBrowserUI>().zoomToPos();
		}
		else if( name == "4") // close the menu
			GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().displayRadialPanel(false);
		else // scroll up and down
			GameObject.Find("BrowserGUI").GetComponent<GUIBrowserUI>().scroll(name);
	}
	else // if this is the move mouse menu
	{
				// mouve the mouse
				// mouve the radial 
				// be careful of edges 	
		if (name=="4")// click
		{

				GameObject.Find("BrowserGUI").GetComponent<GUIBrowserUI>().click();
			GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().displayRadialPanel(false);


		}
		else
		{
			string coordonates;
			if(name =="0")
				coordonates = "0;-10";
			else if(name =="1")
				coordonates = "10;0";
			else if(name =="2")
				coordonates = "0;10";
			else 
				coordonates = "-10;0";
			GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>().mouseUpdatePosition(coordonates);

		}
	}
}




}







[System.Serializable]
public class MenuButton
{
	public string name;
	public Image sceneimage;
	public Color NormalColor = Color.white;
	public Color HighlightedColor = Color.red;
	public Color PressedColor = Color.blue;
}

