﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Tobii.Gaming;
using System;
using System.IO;

using System.Runtime.InteropServices;
using ZenFulcrum.EmbeddedBrowser;
using UnityEngine.SceneManagement;

public class menuData : MonoBehaviour {

	public GameObject browserGUI;
	public GameObject eyeTracker;
	public GameObject menu;
	public GameObject keyboard;
	private bool saveKeyboard;
    private GameObject savePanel; 
    public GameObject scroll;

	// Use this for initialization
    void Start () {

// main, radius, radial, circular
  }

	// Update is called once per frame
  void Update () {
    }

public void displayMenu()
{
            menu.SetActive(!menu.active);
            browserGUI.SetActive(!menu.active);
            if(menu.active)
            {
                
               saveKeyboard= keyboard.active;
               keyboard.SetActive(false);
               if(GameObject.Find("right_panel"))
               {
                    savePanel=GameObject.Find("right_panel");
                    GameObject.Find("right_panel").SetActive(false);
                }
                else if(GameObject.Find("moving_panel"))
                {
                    savePanel=GameObject.Find("moving_panel");
                    GameObject.Find("moving_panel").SetActive(false);
                }
                else 
                {
                    savePanel=null;
                }
            }
            else
            {
                
                if(savePanel!=null)
                savePanel.SetActive(true);
                keyboard.SetActive(saveKeyboard);
            }

         if(scroll!=null)
        {
            scroll.SetActive(!scroll.active);
        }
}
public void backToMenu()
{
                    GameObject.Find("settings").GetComponent<settings>().savePreferencesBrowser(); 
}

}
