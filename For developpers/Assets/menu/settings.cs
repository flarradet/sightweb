﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json ;
using System.IO;
using ZenFulcrum.EmbeddedBrowser;
using UnityEngine.SceneManagement;
using System; 

public class settings : MonoBehaviour {
	public savePrefs sp;
      [HideInInspector]
  public GameObject browserObject;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(this.gameObject);
		loadPreferences();
	}


    public void savePreferencesBrowser()
    {
    	browserPrefs browser= new browserPrefs();
    	eye_tracker_browser ey= GameObject.Find("eyeTracker").GetComponent<eye_tracker_browser>();
        browser.time_to_wait_main=ey.time_to_wait_main;
        browser.time_to_wait_menu=ey.time_to_wait_menu;
        browser.time_to_wait_radialButtons=ey.time_to_wait_radialButtons;

        browser.radius_menu=ey.radius_menu;
        browser.displayBall=ey.displayBall;
		    browserObject.SetActive(true);
        IPromise<List<Cookie>> tmp= browserObject.GetComponent<Browser>().CookieManager.GetCookies();
		    tmp.Then(result =>saveAndExit(result) );
		    browser.localStorage=browserObject.GetComponent<Browser>().localStorage; 
        sp.browser = browser;      
    } 
    public void saveAndExit(List<Cookie> cookieList)
    {
    	//print("yooo"+cookieList.Count)  ;  
    	sp.browser.cookies= cookieList ;
    	//Application.LoadLevel("menu");
              SceneManager.LoadScene("menu");

    }
        public void savePreferencesSpeech()
    {
    	  speechPrefs speech= new speechPrefs();
       	eye_tracker ey= GameObject.Find("eyeTracker").GetComponent<eye_tracker>();
        speech.time_to_wait_main =ey.time_to_wait_main;
        speech.displayBall=ey.displayBall;
        sp.speech = speech;
        //Application.LoadLevel("menu");
        SceneManager.LoadScene("menu");

    } 
            public void savePreferences()
    {
       // browser.localStorage=null;
        string json = JsonConvert.SerializeObject(sp);


        StreamWriter writer = new StreamWriter("playerPrefs.txt", false);
        writer.WriteLine(json);
        writer.Close();

    }
        public void loadPreferences()
    {
      if(sp == null) // if we havent already load the preferences 
      {
        try
        {
      		string json;
          StreamReader reader = new StreamReader("playerPrefs.txt"); 
          json= reader.ReadToEnd();
          reader.Close();

           sp = JsonConvert.DeserializeObject<savePrefs>(json);
         }catch(Exception e)
         {        

            createDefault();
         }
         if( sp.offsets == null)
         {
           // Application.LoadLevel("calibration");
                  SceneManager.LoadScene("calibration");

         }
         else
         {
                //   Application.LoadLevel("menu");
                  SceneManager.LoadScene("menu");

   
         }
       }
    } 
    public void createDefault()
    {
      print("createDefault");
         sp = new savePrefs();
        browserPrefs b = new browserPrefs();
        speechPrefs s = new speechPrefs();
        b.time_to_wait_main=1.0f;
        b.time_to_wait_menu = 1f;
        b.time_to_wait_radialButtons=1.0f;
        b.displayBall=false;
        b.radius_menu=15f;

        s.time_to_wait_main=1.0f;
        s.displayBall=false;

        sp.browser=b;
        sp.speech = s;
        sp.offsets =  new Vector2[5]{new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(0,0)};
        sp.meanOffsets = new Vector2(0,0);

    }


public class savePrefs {
   public browserPrefs browser;
   public speechPrefs speech;
   public Vector2[] offsets;
   public Vector2 meanOffsets;
}

public class browserPrefs {
   public float time_to_wait_main;
   public float time_to_wait_menu;
   public float time_to_wait_radialButtons;
   public  float radius_menu;
   public  bool displayBall;
   public List<Cookie> cookies; 
   public  Dictionary<string,List<KeyValuePair<string,string>> >  localStorage;

}

public class speechPrefs {
   public float time_to_wait_main;
   public float time_to_wait_autocompletion;
   public bool displayBall;
}
}
