﻿


using System.Collections;
using System.Collections.Generic;

using UnityEngine;
//using Tobii.EyeTracking;
using Tobii.Gaming;
using UnityEngine.UI;
// Runtime code here
#if UNITY_EDITOR
using UnityEditor;
#endif
using System.IO;
using ZenFulcrum.EmbeddedBrowser;
using System.Runtime.InteropServices;
using howto_bounding_circle;
using System.Drawing;
using UnityEngine.SceneManagement;
using System;

public class eye_tracker_menu : Eye_tracker_interface {
    float camRayLength = float.PositiveInfinity;
    public float time_to_wait_calibration { get; set; }


    void Start () {

      setup();
        gazePlot.SetActive(displayBall);
  }

    // Update is called once per frame
  void Update()
  {
    resizeWindow();


    gazeAction();

}

public Vector3 Smoothify3(Vector3 point,float FilterSmoothingFactor )
{
    if (!_hasHistoricPoint)
    {
        _hasHistoricPoint = true;
        _historicPoint=point;
        return point;
    }

    var smoothedPoint = new Vector3(
      point.x * (1.0f - FilterSmoothingFactor) + _historicPoint.x * FilterSmoothingFactor,
      point.y * (1.0f - FilterSmoothingFactor) + _historicPoint.y * FilterSmoothingFactor,
      point.z * (1.0f - FilterSmoothingFactor) + _historicPoint.z * FilterSmoothingFactor);

    _historicPoint=smoothedPoint;
    return smoothedPoint;
}
void loadBrowser()
{
        print("default");
        print(PlayerPrefs.GetString("defaultWebsite"));
        // Application.LoadLevel("Browser_"+PlayerPrefs.GetString("version"));     
        SceneManager.LoadScene("Browser_"+PlayerPrefs.GetString("version"));
}

public override void getFinalPoint()
{
    float h = Screen.height;
    float w = Screen.width;
    gazePosition = new Vector3(gazePosition.x - meanOffsets[0] , gazePosition.y - meanOffsets[1] , 0.0f);

        if( gazePosition.x < w/2 && gazePosition.y > h/2){
        index =1;
    }
    else if( (gazePosition.x > w/2 ) && gazePosition.y > h/2){
        index=2;
    }
    else if(gazePosition.x < w/2 && (gazePosition.y < h/2)){
        index=3;
    }
    else{
        index=4;
    }


    pointFixed = new Vector3(gazePosition.x-offsets[index][0]  ,gazePosition.y - offsets[index][1],gazePosition.z);

    if(SceneManager.GetActiveScene().name!="calibration"){ //this is the menu
        smoothedPoint =  Util.Smoothify3(pointFixed,0.97f)[1]; 
        toRay= smoothedPoint;
    }
    else // if  if we are in calibration mode dont smoothed 
    {
        smoothedPoint =  Util.Smoothify3(gazePosition,0.97f)[1]; 
        smoothedFixedPoint =  Smoothify3(pointFixed,0.97f); 
        toRay= smoothedFixedPoint;
    }

    if(newOffset){
        camRay = cameraGUI.ScreenPointToRay(pointFixed);
        newOffset=false;
    }
    else
    camRay  = cameraGUI.ScreenPointToRay(toRay);
}

public override void doActionButton()
{


    if(hit.collider.gameObject.name=="browser")
    {
            string[] lines = System.IO.File.ReadAllLines(@"preferences.txt");
            string version = lines[6].Split(new String[] { "=" }, StringSplitOptions.None)[1];
            PlayerPrefs.SetString("version", version);
       // Application.LoadLevel("menu_browser"); 
            SceneManager.LoadScene("favorite");

    }
    else if(hit.collider.gameObject.name=="speak")
    {
        SceneManager.LoadScene("keyboard");

    }
      else if(hit.collider.gameObject.name=="game")
    {
        SceneManager.LoadScene("_Complete-Game");

    }
    else if(hit.collider.gameObject.name=="google")
    {

            string[] lines = System.IO.File.ReadAllLines(@"preferences.txt");
            string searchEngine = lines[0].Split(new string[] { "=" }, StringSplitOptions.None)[1];


            PlayerPrefs.SetString("defaultWebsite", searchEngine);

            loadBrowser();
        }
    else if(hit.collider.gameObject.name=="whatsapp")
    {
        PlayerPrefs.SetString("defaultWebsite","https://web.whatsapp.com/");
        loadBrowser();
    } 
    else if(hit.collider.gameObject.name=="news")
    {
        PlayerPrefs.SetString("defaultWebsite","https://news.google.it");
        loadBrowser();
    }    
    else if(hit.collider.gameObject.name=="calibration")
    {
      //  Application.LoadLevel("calibration"); 
                SceneManager.LoadScene("calibration");

                            //Application.LoadLevel("test"); 

    }     
    else if(hit.collider.gameObject.name=="Back")
    {
       // Application.LoadLevel("menu"); 
        SceneManager.LoadScene("menu");

    }     
    else if(hit.collider.gameObject.name=="close")
    {
        GameObject.Find("settings").GetComponent<settings>().savePreferences();
        while(ShowCursor(true) <0){}

                            #if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
                             #else
        Application.Quit();
                             #endif
    }        

    else if(hit.collider.gameObject.name.Substring(0,1)=="v")
    {
        PlayerPrefs.SetString("version",hit.collider.gameObject.name);
       // Application.LoadLevel("favorite");
            SceneManager.LoadScene("favorite");


    }
}

public override void startConsideringKey()
{
    if(hit.collider.gameObject.name!="Sphere1")
        timer.enabled = true;   

    timer.transform.position =  new Vector3(  hit.collider.gameObject.transform.position.x , hit.collider.gameObject.transform.position.y, hit.collider.gameObject.transform.position.z);
}

public override void ActionsLookNothing()
{
 
}
public override void ActionWhenLookAtSomething()
{
   
}

public override void getTime_to_wait()
{
    if(hit.collider.gameObject.name=="Sphere1")
        time_to_wait= time_to_wait_calibration;
    else
        time_to_wait= time_to_wait_main;
}


public override void getDataFromPlayerPrefs()
{

        time_to_wait_main = 2.0f;
        time_to_wait_calibration= 2.2f;

        // center, top left, top right, bottom left, bottom right
        if( GameObject.Find("settings") && SceneManager.GetActiveScene().name!="calibration"){
            offsets =  GameObject.Find("settings").GetComponent<settings>().sp.offsets;
            meanOffsets =  GameObject.Find("settings").GetComponent<settings>().sp.meanOffsets;
        }
        else{
            offsets= new Vector2[5]{new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(0,0),new Vector2(0,0)};
            meanOffsets =  new Vector2(0,0);
        }
    newOffset=false;
}
    public void resizeWindow()  
    {
        float size=  Math.Min(0.00085f * Screen.width , 0.002f * Screen.height);
        GameObject.Find("wholeWindow").transform.localScale = new Vector3(size ,size, 1.0f);
    }

}
